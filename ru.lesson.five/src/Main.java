import lists.IArrayList;
import lists.ILinkedList;

import java.util.ArrayList;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        // Тест Linked List
        System.out.println("Test linked list:");
        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("a");
        linkedList.add(0, "a");
        linkedList.add(null);
        linkedList.add("d");
        linkedList.add(1, "b");
        linkedList.remove("d");
        linkedList.remove(3);
        linkedList.set(2, "c");
        for (int i = 0; i < linkedList.size(); i++) {
            System.out.println("linkedList at " + i + ": " + linkedList.get(i));
        }

        ILinkedList<String> linkedListMy = new ILinkedList<>();
        linkedListMy.add("a");
        linkedListMy.add(0, "a");
        linkedListMy.add(null);
        linkedListMy.add("d");
        linkedListMy.add(1, "b");
        linkedListMy.remove("d");
        linkedListMy.remove(3);
        linkedListMy.set(2, "c");
        for (int i = 0; i < linkedListMy.size(); i++) {
            System.out.println("linkedListMy at " + i + ": " + linkedListMy.get(i) );
        }

        System.out.println("\nTest Array list:");
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("a");
        arrayList.add(0, "a");
        arrayList.add(null);
        arrayList.add("d");
        arrayList.add(1, "b");
        arrayList.remove("d");
        arrayList.remove(3);
        arrayList.set(2, "c");
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println("arrayList at " + i + ": " + arrayList.get(i));
        }

        IArrayList<String> arrayListMy = new IArrayList<>();
        arrayListMy.add("a");
        arrayListMy.add(0, "a");
        arrayListMy.add(null);
        arrayListMy.add("d");
        arrayListMy.add(1, "b");
        arrayListMy.remove("d");
        arrayListMy.remove(3);
        arrayListMy.set(2, "c");
        for (int i = 0; i < arrayListMy.size(); i++) {
            System.out.println("arrayListMy at " + i + ": " + arrayListMy.get(i));
        }

    }
}
