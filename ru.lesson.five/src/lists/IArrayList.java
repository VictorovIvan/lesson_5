package lists;

import java.util.Arrays;
import java.util.Objects;

public class IArrayList<T> implements IList {
    protected transient int modCnt = 0;
    transient Object[] elData;
    private int size;
    private static final Object[] DEF_CAP_EMPTY_EL_DATA = {};
    private static final int DEF_CAP = 10;
    public static final int MAX_ARR_LENGTH = Integer.MAX_VALUE - 8;
    private static final Object[] EMPTY_EL_DATA = {};


    public IArrayList() {
        this.elData = EMPTY_EL_DATA;
    }

    @Override
    public void add(Object o) {
        modCnt++;
        add((T)o, elData, size);
    }

    @Override
    public void add(int i, Object o) {
        checkRangeForAdd(i);
        modCnt++;
        final int val;
        Object[] el;
        if ((val = size) == (el = this.elData).length)
            el = upVal();
        System.arraycopy(el, i, el, i + 1, val - i);
        el[i] = o;
        size = val + 1;
    }

    @Override
    public void remove(int i) {
        Objects.checkIndex(i, size);
        final Object[] es = elData;
        fastRemove(es, i);
    }

    @Override
    public void remove(Object o) {
        final Object[] es = elData;
        final int size = this.size;
        int i = 0;
        found:
        {
            if (o == null) {
                for (; i < size; i++)
                    if (es[i] == null)
                        break found;
            } else {
                for (; i < size; i++)
                    if (o.equals(es[i]))
                        break found;
            }
        }
        fastRemove(es, i);
    }

    @Override
    public void set(int index, Object o) {
        Objects.checkIndex(index, size);
        elData[index] = o;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Object get(int index) {
        return elementData(index);
    }

    T elementData(int index) {
        return (T)elData[index];
    }


    private void add(T val, Object[] elData, int sizeData) {
        if (sizeData == elData.length)
            elData = upVal();
        elData[sizeData] = val;
        size = sizeData + 1;
    }

    private Object[] upVal() {
        return upVal(size + 1);
    }

    private Object[] upVal(int minCap) {
        int oldCap = elData.length;
        if (oldCap > 0 || elData != DEF_CAP_EMPTY_EL_DATA) {
            int newCap = newLength(oldCap, minCap - oldCap, oldCap >> 1);
            return elData = Arrays.copyOf(elData, newCap);
        } else {
            return elData = new Object[Math.max(DEF_CAP, minCap)];
        }
    }

    public static int newLength(int oldValLgth, int minValGrowth, int prefValGrowth) {

        int newLength = Math.max(minValGrowth, prefValGrowth) + oldValLgth;
        if (newLength - MAX_ARR_LENGTH <= 0) {
            return newLength;
        }
        return hugeLength(oldValLgth, minValGrowth);
    }

    private static int hugeLength(int oldLgth, int minValGrowth) {
        int minLength = oldLgth + minValGrowth;
        if (minLength < 0) { // overflow
            throw new OutOfMemoryError("Out of memory");
        }
        if (minLength <= MAX_ARR_LENGTH) {
            return MAX_ARR_LENGTH;
        }
        return Integer.MAX_VALUE;
    }

    private void checkRangeForAdd(int index) {
        if (index > size || index < 0)
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
    }

    private void fastRemove(Object[] val, int i) {
        modCnt++;
        final int newSize;
        if ((newSize = size - 1) > i)
            System.arraycopy(val, i + 1, val, i, newSize - i);
        val[size = newSize] = null;
    }
}
