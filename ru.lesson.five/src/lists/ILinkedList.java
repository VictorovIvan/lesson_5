package lists;

public class ILinkedList<T> implements IList {
    transient int size = 0;
    protected transient int modCount = 0;
    transient Node<T> firstNode;
    transient Node<T> lastNode;

    @Override
    public void add(Object o) {
        linkToLastList((T)o);
    }

    @Override
    public void add(int index, Object o) {
        if (index != size) {
            linkToBeforeList((T)o, node(index));
        } else {
            linkToLastList((T)o);
        }
    }

    @Override
    public void remove(int index) {
        unlinkFromList(node(index));
    }

    @Override
    public void remove(Object o) {
        if (o != null) {
            for (Node<T> node = firstNode; node != null; node = node.next) {
                if (o.equals(node.item)) {
                    unlinkFromList(node);
                }
            }
        } else {
            for (Node<T> node = firstNode; node != null; node = node.next) {
                if (node.item == null) {
                    unlinkFromList(node);
                }
            }
        }
    }

    @Override
    public void set(int index, Object o) {
        Node<T> node = node(index);
        node.item = (T)o;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Object get(int index) {
        return node(index).item;
    }

    void linkToLastList(T e) {
        final Node<T> last = lastNode;
        final Node<T> newNode = new Node<>(last, e, null);
        lastNode = newNode;
        modCount++;
        size++;
        if (last != null) {
            last.next = newNode;
        } else {
            firstNode = newNode;
        }
    }

    void linkToBeforeList(T e, Node<T> s) {
        final Node<T> previous = s.prev;
        final Node<T> newNode = new Node<>(previous, e, s);
        s.prev = newNode;
        size++;
        modCount++;
        if (previous != null) {
            previous.next = newNode;
        }
        else {
            firstNode = newNode;
        }
    }

    Node<T> node(int indx) {
        if (indx < (size >> 1)) {
            Node<T> val = firstNode;
            for (int cnt = 0; cnt < indx; cnt++)
                val = val.next;
            return val;
        } else {
            Node<T> val = lastNode;
            for (int cnt = size - 1; cnt > indx; cnt--)
                val = val.prev;
            return val;
        }
    }

    T unlinkFromList(Node<T> node) {
        final T el = node.item;
        final Node<T> next = node.next;
        final Node<T> previous = node.prev;

        node.item = null;
        size--;
        modCount++;

        if (previous == null) {
            firstNode = next;
        } else {
            previous.next = next;
            node.prev = null;
        }
        if (next == null) {
            lastNode = previous;
        } else {
            next.prev = previous;
            node.next = null;
        }
        return el;
    }
}
