package lists;

class Node<T> {
    T item;
    Node<T> next;
    Node<T> prev;

    Node(Node<T> previous, T el, Node<T> next) {
        this.item = el;
        this.next = next;
        this.prev = previous;
    }
}
